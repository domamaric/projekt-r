from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

ENV = 'prod'

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'projekt'
    if ENV == 'dev':
        app.debug = True
        app.config[
            'SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:bazepodataka' \
                                         '@localhost/projekt-r-db '
    else:
        app.debug = False
        app.config[
            'SQLALCHEMY_DATABASE_URI'] = 'postgresql://svcahffulwgovq:c87a5198fc733663ee64df3df12c89f13a06a183f89079abceaf461a39d30fbf@ec2-18-203-64-130.eu-west-1.compute.amazonaws.com:5432/dkdhrhgcn0gtp'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    from .views import views
    from .auth import AUTH

    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(AUTH, url_prefix='/')

    db = SQLAlchemy(app)

    with app.app_context():
        db.create_all()

    from .models import User

    # create_database(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    return app
