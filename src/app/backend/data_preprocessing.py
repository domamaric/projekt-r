import re
import string

from nltk.corpus import stopwords, words
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer

ALL_WORDS = set(words.words())


def process(text):
    text = text.lower()
    text = remove_rt(text)
    text = remove_url(text)
    text = remove_username(text)
    text = remove_numbers(text)
    text = remove_punctuation(text)
    # tokenizes before removing punctutaion to get word.sentence as two words
    # and not as wordsentence
    tokens = tokenize_text(text)
    tokens = [remove_punctuation(token) for token in tokens]
    tokens = remove_repeating(tokens)
    tokens = remove_stopwords(tokens)
    tokens = word_lemmantizer(tokens)
    return " ".join([t for t in tokens])


def remove_punctuation(text):
    no_punct = "".join([c for c in text if c not in string.punctuation])
    return no_punct


def tokenize_text(text):
    tokenizer = RegexpTokenizer(r'\w+')
    return tokenizer.tokenize(text)


def remove_url(text):
    return re.sub('((www.[^s]+)|(https?://[^s]+))', ' ', text)


def remove_numbers(text):
    return re.sub('[0-9]+', '', text)


def remove_rt(text):
    return re.sub('RT @', '@', text)


def remove_username(text):
    return re.sub('@([A-Za-z0-9_]+)', '', text)


def remove_stopwords(text):
    return [w for w in text if w not in stopwords.words('english')]


def word_lemmantizer(text):
    lemmantizer = WordNetLemmatizer()
    return [lemmantizer.lemmatize(w) for w in text]


def word_stemmer(text):
    stemmer = PorterStemmer()
    return [stemmer.stem(w) for w in text]


def remove_repeating(text):
    tokens2 = []
    # try pyenchant for better results

    for w in text:
        word2 = re.sub(r'(.)\1+', r'\1\1', w)
        if w != word2:
            if word2 in ALL_WORDS:
                tokens2.append(w)
            else:
                tokens2.append(w)
        else:
            tokens2.append(w)

    return tokens2

# ?? remove single letters from words?????
