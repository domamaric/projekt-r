from datetime import date
from datetime import datetime

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required

from tweets.getTweets import get_tweets, user_exists

views = Blueprint('views', __name__)


@views.route('/', methods=['GET', 'POST'])
@login_required
def home():
    if request.method == 'POST':
        user = request.form.get('user')
        number = request.form.get('number')
        form_datetime = request.form.get('date')
        if form_datetime == '':
            flash('Date must be selected', category='error')
            return render_template("home.html", user=current_user)

        else:
            form_date = datetime.fromisoformat(form_datetime)
        if number == "":
            flash('Number of Tweets not selected', category='error')
        elif int(number) <= 0:
            flash('Number of Tweets must be greater than 0.', category='error')
        elif not user_exists(user):
            flash('This user does not exist', category='error')
        elif form_date.date() > date.today():
            flash('Date must be in the past', category='error')
        else:
            tweets = get_tweets(user, number, form_date)
            # tw = dumps(tweets)
            flash({
                'tweets': tweets,
                'number': number,
                'date': form_datetime,
                'user': user, }, category='tweets')
            return redirect(url_for('views.stats'))

    return render_template("home.html", user=current_user)


@views.route('/stats')
@login_required
def stats():
    return render_template("stats.html", user=current_user)
