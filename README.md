# Projekt R

Projekt R jedan je od obaveznih predmeta na preddiplomskome studiju [**FER**](https://www.fer.unizg.hr/predmet/pro/o_predmetu)-a. Studenti rade na projektima različitih tema u grupi od 3 ili više studenata, u pravilu pod vodstvom svog mentora.

## Opis problematike

**Twitter** je društvena mreža na kojoj korisnici objavljuju poruke (tzv. tvitove) oko stvari iz života. Tvitovi obično sugeriraju neko emocionalno stanje korisnika koji ih objavljuje (npr. :smile:, :cry:, :angry:). U najjednostavnijoj se varijanti, može ustanoviti polaritet poruke po pitanju iskazanih emocija.

## Tema izrade

Tema našeg je rada razviti web aplikaciju koja će pomoću čitanja podataka iz tokova podataka (engl. data streams) s Twittera dobavljati i analizirati sentiment javnih poruka od određenih korisnika.

## Zahtjevi sustava

Sustav se sastoji od 3 dijela. Prvi dio služi za definiranje korisnika za kojeg se žele dohvatiti javne poruke, broj tvitova i period iz kojeg želimo dohvatiti tvitove. U drugome je dijelu ostvareno strojno učenje korištenjem **LSTM** arhitekture [**RNN**](https://en.wikipedia.org/wiki/Recurrent_neural_network) mreže. Treći dio aplikacije izvještava korisnika o rezultatima analize sentimenta tako da ispiše brojčane rezultate vrednovanja modela i grafičkom vizualizacijom sentimenta pristiglih poruka.

## Nefunkcionalni zahtjevi

Frontend i backend aplikacije su rađeni programskim jezikom Python, detaljnije model je neuronske mreže izrađen korištenjem knjižnice **Keras** paketa **TensorFlow**, a prikaz na klijentskoj je strani ostvaren korištenjem modula **Flask**. Web aplikacija potpuno je responzivna i dizajnirana principom *mobile first*. Rad sa sustavom je osiguran putem HTTPS protokola te korištenjem registracije i potom prijave ovlaštenih korisnika.

## Korištenje aplikacije

Aplikacija je dostupna na javnome poslužitelju Heroku na [adresi](https://fer-twitter-sentiment-analysis.herokuapp.com/login). 
> Aplikaciju je također moguće preuzeti i pokrenuti ju lokalno.
```
git clone https://gitlab.com/domamaric/projekt-r.git
cd projekt-r/src/app
python main.py
```

## Budući rad

- [ ] Prepraviti model neuronske mreže da klasificira više emocija
- [ ] Pojednostavniti korisničko sučelje aplikacije

## Zaključak 

Posebno bih se želio zahvaliti Ivani Stilinović, Katarini Bošnjak i Domagoju Matoševiću za doprinos i rad na aplikaciji :clap:.
