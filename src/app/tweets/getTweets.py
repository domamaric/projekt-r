import json
from pathlib import Path

import pandas as pd
import pytz
import tweepy as tw
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer, tokenizer_from_json

from backend.data_preprocessing import process

# paziljivo s ovim podacima
CONSUMER_KEY = "FRI1f2NyKOvdV4ZrljYsRUbeP"
CONSUMER_SECRET = "ZXzacbt7YSu3vnudAv0FgR0PUfX4VnXnfP87oJ1zbcaeFKEZ7h"
ACCESS_TOKEN = "1457773767061499906-wC3MdTAnVVWL7wlChEpWW8A41pzggG"
ACCESS_TOKEN_SECRET = "rXehvXSIlIrYYNW0GT3DUkUEfadDaRHnlbfEmb6xRACgv"

AUTH = tw.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
AUTH.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
API = tw.API(AUTH)

SENTIMENT = [[0, 1], ['positive', 'negative']]
UTC = pytz.UTC
TOKENIZER_CONFIG = Path(__file__).parent.parent / "backend/tmp" \
                                                  "/tokenizer_config.json "
LOADED_MODEL = Path(__file__).parent.parent / "backend/tmp/model.h5"
TOKENIZER = Tokenizer(num_words=20000)

with open(TOKENIZER_CONFIG) as json_file:
    data = json.load(json_file)
    TOKENIZER = tokenizer_from_json(json.dumps(data))


def get_tweets(user, number, startDate):
    tweets = []

    tweets_display = []

    # prikoupi tweetove odn idi kroz timeline usera 
    for i in tw.Cursor(API.user_timeline, id=user, tweet_mode="extended").items(
            int(number)):
        # pogledaj kad je tweet stvoren ako je stariji od trazenog prekini
        if i.created_at < UTC.localize(startDate):
            break
        # dodaj tweet u polje 
        tweets.append(i.full_text)

    df = pd.DataFrame(data=tweets, columns=['tweets'])

    model = load_model(LOADED_MODEL)
    print(model.summary())

    for tweet in tweets:
        tweet_d = [tweet, predict_sentiment_preprocess(tweet, model)]
        tweets_display.append(tweet_d)

    print(df)
    # print(model.history.history)

    return tweets_display


def predict_sentiment_preprocess(text, model):
    text = process(text)

    if text == '' or text.isspace():
        return 'Not enough data.'

    tw = TOKENIZER.texts_to_sequences([text])
    tw = pad_sequences(tw, maxlen=200)
    prediction = int(model.predict(tw).round().item())

    print("Predicted label: ", SENTIMENT[1][prediction])

    return SENTIMENT[1][prediction]


def user_exists(user):
    # see if user exists, if yes retrun True else False
    try:
        x = API.get_user(screen_name=user)
        return True
    except UserWarning:
        return False
